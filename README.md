# Vue.js Github Search #

This apps polls the Github Search API to find the most starred Vue.js projects created in the past week.

## Tooling: ##

* Bower - dependency management
* Gulp - build automation

NB: bower_components and node_modules are committed to the repo as well as package.json, just for the one-off nature of the project; this should mean the package is installable by cloning and running the gulp 'serve' task in the project root folder


## Dependencies: ##

* Vue.js - view library
* Bourbon/Neat/Bitters - SASS design pattern framework/mixin library